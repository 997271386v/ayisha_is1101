#include <stdio.h>
int main () {
	double a, b, product;
	printf ("enter two numbers: ");
	scanf ("%1f %1f" , &a, &b);
	
	//Calculating product
	product = a*b;
	
	//Result up to 2 decimal point is displayed using %.21f
	printf ("product = %.21f" , product);
	
	return 0;
}